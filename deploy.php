<?php
namespace Deployer;

require 'recipe/common.php';

// Project repository
set('repository', 'git@gitlab.com:Lexinek/nette-camp-deployer.git');

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('dev.nettecamp.entry.do')
	->stage('dev')
	->user('root')
	->forwardAgent(false)
	->addSshOption('StrictHostKeyChecking', 'no')
	->set('deploy_path', '/var/deployer/dev.nettecamp.entry.do');

host('prod.nettecamp.entry.do')
	->stage('prod')
	->user('root')
	->forwardAgent(false)
	->addSshOption('StrictHostKeyChecking', 'no')
	->set('deploy_path', '/var/deployer/prod.nettecamp.entry.do');
    

// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
